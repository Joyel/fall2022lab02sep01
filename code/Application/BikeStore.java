//Joyel Selvakumar, 2111271

package Application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bikeGroup = new Bicycle[4];

        bikeGroup[0] = new Bicycle("Supercycle", 7, 15);
        bikeGroup[1] = new Bicycle("Kona", 8, 20);
        bikeGroup[2] = new Bicycle("Specialized", 10, 25);
        bikeGroup[3] = new Bicycle("Cervelo", 12, 30);

        for (int i = 0; i < bikeGroup.length; i++){
            System.out.println(bikeGroup[i]);
        }

    }
}
